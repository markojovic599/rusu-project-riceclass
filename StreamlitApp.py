#!/usr/bin/env python
# coding: utf-8

# In[10]:


import streamlit as st
import tensorflow as tf
from PIL import Image
from tensorflow import keras 


# In[13]:


# Load the pre-trained Keras model
num_label = 5 # number of labels
model = tf.keras.models.load_model('RiceModel.h5')


# Define a function to preprocess the uploaded image
def preprocess_image(image):
    # Resize the image to 224x224
    image = image.resize((224, 224))
    # Convert the PIL image to a NumPy array
    image = tf.keras.preprocessing.image.img_to_array(image)
    # Normalize the pixel values to [0, 1]
    image = image / 255.0
    # Expand the dimensions of the array to match the input shape of the model
    image = tf.expand_dims(image, axis=0)
    return image

# Define the Streamlit app
def app():
    # Set the title and the sidebar
   
    st.sidebar.title('Upload Image')
    # Allow the user to upload an image file
    uploaded_file = st.sidebar.file_uploader(label='Choose an image', type=['jpg', 'jpeg', 'png'])
    # If an image file was uploaded, display the image and make a prediction
    if uploaded_file is not None:
        # Open and display the uploaded image
        image = Image.open(uploaded_file)
        st.image(image, caption='Uploaded Image', use_column_width=True)
        # Preprocess the image and make a prediction using the Keras model
        x = preprocess_image(image)
        predictions = model.predict(x)
        # Get the predicted class label and the corresponding probability
        predicted_label = tf.argmax(predictions, axis=1)[0]
        probability = tf.nn.softmax(predictions)[0][predicted_label]
        # Display the predicted class label and the corresponding probability
        st.write(f'Prediction: Class {predicted_label} with probability {probability:.2f}')

# Run the app
if __name__ == '__main__':
    app()


# In[11]:





# In[12]:





# In[ ]:




